
let password=null;

if (process.argv.slice(2) == undefined || process.argv.slice(2).length < 1) {
  console.log("Invalid Request! \nPLAIN_TEXT Not provided")
  process.exit();
}


else if (process.argv.slice(2).length >= 2 && process.argv[3] !=undefined) {
  console.log("Using the provided Passwored.");
  password = process.argv[3].trim();
}else{
  console.log("Generating a random Passwored.");
  password= Array(64).fill("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz").map(function(x) { return x[Math.floor(Math.random() * x.length)] }).join('');
}

var crypto = require('crypto'),
  algorithm = 'aes256';

  let PLAIN_TEXT = `${process.argv[2]}`;

function encrypt(text) {
  var cipher = crypto.createCipher(algorithm, password)
  var crypted = cipher.update(text, 'utf8', 'hex')
  crypted += cipher.final('hex');
  return crypted;
}

function decrypt(text) {
  var decipher = crypto.createDecipher(algorithm, password)
  var dec = decipher.update(text, 'hex', 'utf8')
  dec += decipher.final('utf8');
  return dec;
}
console.log(`PLAIN_TEXT:  ${process.argv[2]} \nCIPER_TEXT: ${encrypt(PLAIN_TEXT)} \nPASSWORD: ${password}`);
