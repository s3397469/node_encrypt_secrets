# node encrypt secrets

Can be used to symmetrically encrypt the secrects that are used during the applicaiton.


# bash_encrypt_folder_content.sh

Bash Script to symmetrically encrypt the content with a given password.

## Command

./encrypt_folder_content.sh FOLDER_PATH

## NOTE

* Once encypted, source file will be deleted.
* Provided password will be saved to a text file.


## Ref:

https://lollyrock.com/posts/nodejs-encryption/


## TODO:

Alter the password to generate a fix lenght irrespective of the source length.
