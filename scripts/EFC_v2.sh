#1/bin/bash

# EFC_v2 [Encrypt Folder Content]
# Version 2.0

# Atomated script to symmetrically encrypt the content of a given folder.
# Features:
## Accept user given path to a folder.
## Request user to enter a password.
## Ceate a zip file of the given folder.
## Encrypt the zip file.
## Generate the MD5 of the ZIP file.
## Create a TXT file containing the MD5, Name and Password in Home directory.


# Input parameters
## Request Password

echo -n Password: 
read -s password

FILE_PATH=$1
basename "$FILE_PATH"
FILE_NAME="$(basename -- $FILE_PATH)".zip
echo "Creating the ZIP file : $FILE_NAME.zip"

zip -r $FILE_NAME $1

md5=`md5sum ${FILE_NAME}`

gpg --quiet --yes --batch --passphrase "$password" -c "$FILE_NAME"
echo "Removing $FILE_NAME"
rm -f "$FILE_NAME"
echo "Processing: $FILE_NAME, Checksum : $md5"

ts=$(date +%s)
echo "Password will be backup in : $ts.txt"
echo "$md5 -> $password" > ~/$ts.txt