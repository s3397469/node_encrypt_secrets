// APP Password decryption lib : V 1.0

module.exports = function(ADMIN_KEY,CIPER_TEXT=[]) {

    var crypto = require('crypto'),
    algorithm = 'aes256',
    password = `${ADMIN_KEY}`;
    let retunPlainText=[];

    CIPER_TEXT.forEach(ciperText =>{
        try{
            let decipher = crypto.createDecipher(algorithm, password);

            var dec = decipher.update(ciperText, 'hex', 'utf8')
            dec += decipher.final('utf8');
            retunPlainText.push(dec);
        }catch(e){
            retunPlainText.push(null);
        }
    });

    return retunPlainText;
};