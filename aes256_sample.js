

if(process.argv.slice(2)==undefined || process.argv.slice(2).length ==0 || process.argv[2].trim().length==0){
    console.log("Invalid Request!")
    process.exit();
}

var crypto = require('crypto'),
    algorithm = 'aes256',
    password = `${process.argv[2]}`;

function encrypt(text){
  var cipher = crypto.createCipher(algorithm,password)
  var crypted = cipher.update(text,'utf8','hex')
  crypted += cipher.final('hex');
  return crypted;
}
 
function decrypt(text){
  var decipher = crypto.createDecipher(algorithm,password)
  var dec = decipher.update(text,'hex','utf8')
  dec += decipher.final('utf8');
  return dec;
}
 
var hw = encrypt("hello world")
console.log(decrypt(hw));